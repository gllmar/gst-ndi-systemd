#!/bin/bash
CONF=~/.config/gst-ndi-send.conf

echo "ARG1=tv" > $CONF
echo "ARG2=scale" >> $CONF

cat $CONF
echo "written to $CONF"

systemctl --user restart gst-ndi-receive