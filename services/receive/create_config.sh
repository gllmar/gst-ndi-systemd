#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

get_ndi_source()
{
	timeout 1s gst-device-monitor-1.0 -f  Source/Network:application/x-ndi | grep ndisrc | awk -F"ndi-name=" '{print substr($0, index($0,$2))}' | cut -d '"' -f 2,4 | sort
}

list_monitor()
{
	export DISPLAY=:0

	# Get monitor information using xrandr
	monitor_info=$(xrandr --query)
	
	# Use awk to filter and print monitor names
	echo "$monitor_info" | awk '/ connected/ {print $1}'
}

echo "Configure gst-ndi-systemd-receive"
read -r -p ">>> Enter instance number: " INSTANCE

echo "NDI SOURCES avaiable"
sources=$(get_ndi_source)
echo "$sources" | nl
read -r -p ">>> Enter source number or name: " source_input
if [[ $source_input =~ ^[0-9]+$ ]] ; then
    SOURCE=$(echo "$sources" | awk "NR==$source_input" | sed -n 's/.*(\(.*\))/\1/p' | tr -d '\\')
else
    SOURCE=$source_input
fi

echo "Connected Display"
monitors=$(list_monitor)
echo "$monitors" | nl
read -r -p ">>> Enter monitor number or name: " monitor_input
if [[ $monitor_input =~ ^[0-9]+$ ]] ; then
    MONITOR=$(echo "$monitors" | awk "NR==$monitor_input")
else
    MONITOR=$monitor_input
fi

read -r -p ">>> Enter Fullscreen (0/1) " FULLSCREEN

config_folder="$HOME/.config/gst-ndi"

# Create the folder if it doesn't exist
mkdir -p "$config_folder"

# Write the configuration file
config_file="$config_folder/receive_$INSTANCE.env"

echo "SOURCE=$SOURCE" > "$config_file"
echo "MONITOR=$MONITOR" >> "$config_file"
echo "FULLSCREEN=$FULLSCREEN" >> "$config_file"

echo "Configuration saved to $config_file"
cat  "$config_file"

systemctl --user enable gst-ndi-receive@"$INSTANCE"
systemctl --user restart gst-ndi-receive@"$INSTANCE"
