#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p ~/.config/systemd/user

echo "====> copying service to user service folder"


SERVICE_NAME=x-wait
cp "$DIR"/$SERVICE_NAME.service ~/.config/systemd/user/$SERVICE_NAME.service
echo "====> reloading systemd daemon"
systemctl --user daemon-reload
systemctl --user restart $SERVICE_NAME
systemctl --user enable $SERVICE_NAME
