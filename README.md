# gst-ndi-service

## Description

Systemd service providing gst-ndi-send and gst-ndi-receive on boot

intended to work on 
* [] Rpi4 32bit
* [x] Rpi4 64bit
* [ ] other..

## Install 

```
cd ~/src/
git clone --recursive https://gitlab.com/gllmar/gst-ndi-systemd/
cd gst-ndi-systemd
./setup.sh
```
## Dependencies

[run setup.sh](./setup.sh)

* NDI SDK 5
* rust
* Gstreamer
* GST NDI 
      * from https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs

## Config

```
config_folder="$HOME/.config/gst-ndi"
```