#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

NDI_SOURCE=$1
FULLSCREEN=$2
MONITOR=$3

determine_session_type() {
    if pgrep -x wayfire >/dev/null; then
        echo "wayland"
    elif pgrep -x mutter >/dev/null || pgrep -x xfwm4 >/dev/null || pgrep -x kwin_x11 >/dev/null; then
        echo "x11"
    else
echo "framebuffer"
        # Disable the cursor on framebuffer
        echo -e '\033[?17;0;0c' > /dev/tty1
        # Ensure cursor gets turned back on when script exits
        trap "echo -e '\033[?17;14;224c' > /dev/tty1" EXIT
    fi
}

SESSION_TYPE=$(determine_session_type)

if [[ $SESSION_TYPE == "wayland" ]]; then
    export WAYLAND_DISPLAY=wayland-1
    export XDG_RUNTIME_DIR="/run/user/$(id -u)"
fi

toggle_fullscreen() {
    if [[ $SESSION_TYPE == "x11" ]]; then
        wmctrl -r "$1" -b toggle,fullscreen
    elif [[ $SESSION_TYPE == "wayland" ]]; then
        echo "toggle_fullscreen function for Wayland is handled in the GST pipeline."
    fi
}

if [ -z "$NDI_SOURCE" ]; then
    echo "No argument supplied, Showing All NDI sources"
    timeout 1s gst-device-monitor-1.0 -f Source/Network:application/x-ndi | grep ndisrc
else
    NDISRC=$(timeout 3s gst-device-monitor-1.0 -f Source/Network:application/x-ndi | grep ndisrc | grep $1 | rev | cut -c5- | rev | xargs)
    echo "$NDISRC"
    NDIURL="${NDISRC#*url-address=}"
    NDIURL=${NDIURL%!}
    echo "NDIURL=" $NDIURL
    [[ ! -z "$NDIURL" ]] && echo "NDI founded" || exit

    PIPELINE="ndisrc url-address=$NDIURL ! ndisrcdemux name=demux demux.video ! queue ! videoconvert"

    if [[ $SESSION_TYPE == "wayland" ]]; then
        echo "Wayland session detected, sink to waylandsink"
        PIPELINE+=" ! waylandsink fullscreen=$FULLSCREEN"
    elif [[ $SESSION_TYPE == "x11" ]]; then
        echo "X11 session detected, sink to autovideosink"
        PIPELINE+=" ! autovideosink"
        [ -n "$FULLSCREEN" ] && [ "$FULLSCREEN" != 0 ] && toggle_fullscreen "gst-launch-1.0" &
    else
        echo "Neither Wayland nor X11 session detected, sink to framebuffer"
                     echo "clear the fb"   
            dd if=/dev/zero of=/dev/fb0
        if [[ -n "$FULLSCREEN" ]] && [[ "$FULLSCREEN" != "0" ]]; then
            IFS=,
            LIST=$(cat /sys/class/graphics/fb0/virtual_size)
            read WIDTH HEIGHT <<<$LIST
            echo "WIDTH=$WIDTH, HEIGHT=$HEIGHT"
            PIPELINE+=" ! videoscale ! video/x-raw,width=$WIDTH,height=$HEIGHT  ! fbdevsink device=/dev/fb0 sync=true"
        else
            PIPELINE+=" ! fbdevsink device=/dev/fb0  sync=true"
        fi
    fi
    echo "Running command: gst-launch-1.0 $PIPELINE"

    #gst-launch-1.0 $PIPELINE
    eval "gst-launch-1.0 $PIPELINE"
fi
