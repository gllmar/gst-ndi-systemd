#!/bin/bash
#SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

export DISPLAY=:0

#cd "$SCRIPTPATH"

gst-launch-1.0 -v \
    v4l2src device=/dev/video0 ! videoconvert ! video/x-raw,format=UYVY ! ndisinkcombiner name=combiner \
    audiotestsrc is-live=true ! combiner.audio \
    combiner.src ! queue ! ndisink ndi-name="My NDI source"


    
#gst-launch-1.0 -v v4l2src device=/dev/video0 ! jpegdec \
#! video/x-raw,width=640,height=360,framerate=60/1 \
#! ndisinkcombiner name=combiner ! \
#ndisink ndi-name="gst-ndi" \
#pulsesrc  ! combiner.audio

