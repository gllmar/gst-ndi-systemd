#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ARCH=$(uname -m)
DEBUG=true

log() {
    if $DEBUG; then
        echo "[DEBUG] $(date): $1"
    fi
}

error_exit() {
    echo "[ERROR] $(date): $1" >&2
    exit 1
}

get_packages() {
    log "Installing required packages."
    sudo apt-get update || error_exit "Failed to update package lists."
    sudo apt-get install -y wmctrl libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
        gstreamer1.0-plugins-base gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly \
        gstreamer1.0-libav libgstrtspserver-1.0-dev libges-1.0-dev \
        libgstreamer-plugins-bad1.0-dev pkg-config libssl-dev \
        gstreamer1.0-plugins-base-apps unclutter || error_exit "Failed to install packages."
}

get_rust() {
    log "Installing Rust."
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y || error_exit "Failed to install Rust."
    source $HOME/.cargo/env || error_exit "Failed to source Rust environment."
}

get_NDI() {
    download_NDI_SDK() {
        log "Downloading NDI SDK for Linux."
        curl -s https://downloads.ndi.tv/SDK/NDI_SDK_Linux/Install_NDI_SDK_v5_Linux.tar.gz | tar xvz -C /tmp/ || error_exit "Failed to download NDI SDK."
    }

    extract_NDI_SDK() {
        log "Extracting NDI SDK for Linux."
        yes y | bash /tmp/Install_NDI_SDK_v5_Linux.sh > /tmp/ndi_install.log 2>&1 || error_exit "Failed to extract NDI SDK. Check /tmp/ndi_install.log for details."
    }

install_NDI_LIB() {
    local extracted_dir="$DIR/lib/NDI SDK for Linux/bin"
    local lib_path="/usr/lib/${ARCH}-linux-gnu/"
    local source_path="${extracted_dir}/${ARCH}-linux-gnu"

    log "Installing NDI libraries to $lib_path."

    if [ -d "$source_path" ]; then
        sudo cp "$source_path"/* "$lib_path" || error_exit "Failed to copy NDI libraries from $source_path."
        sudo ldconfig || error_exit "Failed to update shared library cache."
    else
        error_exit "Expected source directory $source_path does not exist."
    fi
}


    run_get() {
        log "Installing NDI SDK."
        download_NDI_SDK
        extract_NDI_SDK
        install_NDI_LIB
    }

    local filename="/usr/lib/${ARCH}-linux-gnu/libndi.so"
    if [ -f "$filename" ]; then
        read -p "The libndi.so already exists. Do you want to install anyway? (y/n): " choice
        if [[ $choice == [yY] ]]; then
            run_get
        else
            log "Skipping libndi.so installation."
        fi
    else
        run_get
    fi
}

install_CARGO_C() {
    log "Installing Cargo-C."
    local total_ram=$(free -m | awk '/^Mem:/{print $2}')
    log "Total RAM: ${total_ram}MB."

    if [[ $total_ram -lt 3072 ]]; then
        log "Compiling Cargo-C with 2 threads (<3GB RAM)."
        ~/.cargo/bin/cargo install cargo-c -j2 || error_exit "Failed to install Cargo-C."
    else
        log "Compiling Cargo-C with all threads (>3GB RAM)."
        ~/.cargo/bin/cargo install cargo-c || error_exit "Failed to install Cargo-C."
    fi
}

install_GST_NDI() {
    log "Installing GStreamer NDI Plugin."
    git submodule update --init --recursive || error_exit "Failed to update git submodules."
    cd "$DIR/lib/gst-plugins-rs/" || error_exit "Failed to navigate to gst-plugins-rs directory."

    log "Building gst-plugin-ndi."
    ~/.cargo/bin/cargo cbuild --release -p gst-plugin-ndi || error_exit "Failed to build gst-plugin-ndi."

    local lib_path="/usr/lib/${ARCH}-linux-gnu/gstreamer-1.0/"
    log "Copying gst-plugin-ndi to $lib_path."
    sudo cp ./target/${ARCH}-unknown-linux-gnu/release/libgstndi.so $lib_path || error_exit "Failed to copy gst-plugin-ndi."
    sudo ldconfig || error_exit "Failed to update shared library cache."
    gst-inspect-1.0 ndi || error_exit "GStreamer NDI Plugin not detected."
}

test_ndi_sources() {
    log "Testing NDI source discovery."
    gst-device-monitor-1.0 -f Source/Network:application/x-ndi | tee /tmp/ndi_sources.log
    if [ ! -s /tmp/ndi_sources.log ]; then
        error_exit "No NDI sources detected. Check network configuration."
    fi
}

install_services() {
    log "Installing systemd services."
    install_x_wait
    install_unclutter
    install_gst_ndi_receive
    install_gst_ndi_send
}

install_x_wait() {
    log "Installing x-wait."
    cd "$DIR/services/x-wait" || error_exit "Failed to navigate to x-wait directory."
    ./install-x-wait.sh || error_exit "Failed to install x-wait."
}

install_unclutter() {
    log "Installing unclutter."
    cd "$DIR/services/unclutter" || error_exit "Failed to navigate to unclutter directory."
    ./install-unclutter.sh || error_exit "Failed to install unclutter."
}

install_gst_ndi_receive() {
    log "Installing gst-ndi-receive."
    cd "$DIR/services/receive" || error_exit "Failed to navigate to gst-ndi-receive directory."
    ./install-gst-ndi-receive.sh || error_exit "Failed to install gst-ndi-receive."
}

install_gst_ndi_send() {
    log "Installing gst-ndi-send."
    cd "$DIR/services/send" || error_exit "Failed to navigate to gst-ndi-send directory."
    ./install-gst-ndi-send.sh || error_exit "Failed to install gst-ndi-send."
}

setup_menu() {
    while true; do
        clear
        echo "Please select an option:"
        echo "0 -> Exit"
        echo "1 -> Install dependencies"
        echo "2 -> Install services"
        echo "3 -> Configure gst-ndi-receive"
        echo "4 -> Configure gst-ndi-send"
        read -p "Enter your choice: " choice

        case $choice in
            0) log "Exiting..."; exit ;;
            1) get_packages; get_rust; get_NDI; install_CARGO_C; install_GST_NDI ;;
            2) install_services ;;
            3) log "Configuring gst-ndi-receive."; "$DIR/services/receive/create_config.sh" ;;
            4) log "Configuring gst-ndi-send."; "$DIR/services/send/create_config.sh" ;;
            *) log "Invalid choice. Please try again." ;;
        esac
    done
}

setup_menu
