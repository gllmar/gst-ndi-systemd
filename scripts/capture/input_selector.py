import subprocess
import re

def get_video_devices():
    result = subprocess.run(['v4l2-ctl', '--list-devices'], stdout=subprocess.PIPE)
    devices_info = re.findall("(.*):\n\t(.*?/dev/video\d+)", result.stdout.decode(), re.MULTILINE)
    return sorted(devices_info, key=lambda x: int(re.search(r"\d+$", x[1]).group()))

def get_supported_formats(device):
    result = subprocess.run(['v4l2-ctl', '--device=' + device, '--list-formats-ext'], stdout=subprocess.PIPE)
    formats = re.findall("'\w+' \((.*?)\)", result.stdout.decode())
    return list(set(formats))

def get_supported_resolutions(device, chosen_format):
    result = subprocess.run(['v4l2-ctl', '--device=' + device, '--list-formats-ext'], stdout=subprocess.PIPE)
    format_section = re.search(f"{chosen_format}((.|\n)*?)(?=\[.*?\]|\\Z)", result.stdout.decode()).group(0)
    resolutions = re.findall("\d+x\d+", format_section)
    return list(set(resolutions))

def get_supported_framerates(device, chosen_format, chosen_resolution):
    result = subprocess.run(['v4l2-ctl', '--device=' + device, '--list-formats-ext'], stdout=subprocess.PIPE)
    format_section = re.search(f"{chosen_format}((.|\n)*?)(?=\[.*?\]|\\Z)", result.stdout.decode()).group(0)
    resolution_section = re.search(chosen_resolution + "((.|\n)*?)(?=\d+x\d+|\Z)", format_section).group(0)
    framerates = re.findall("\d+\.\d+-\d+\.\d+ fps|\d+\.\d+ fps", resolution_section)
    return framerates

def main():
    devices = get_video_devices()
    for i, device_info in enumerate(devices):
        print(f"{i+1}. {device_info[0]} ({device_info[1]})")

    device_choice = input("Choose a device by number: ")
    chosen_device = devices[int(device_choice)-1][1]

    formats = sorted(get_supported_formats(chosen_device))
    for i, format in enumerate(formats):
        print(f"{i+1}. {format}")

    format_choice = input("Choose a format by number: ")
    chosen_format = formats[int(format_choice)-1]

    resolutions = sorted(get_supported_resolutions(chosen_device, chosen_format), key=lambda x: list(map(int, x.split('x'))), reverse=True)
    for i, resolution in enumerate(resolutions):
        print(f"{i+1}. {resolution}")

    resolution_choice = input("Choose a resolution by number: ")
    chosen_resolution = resolutions[int(resolution_choice)-1]

    framerates = get_supported_framerates(chosen_device, chosen_format, chosen_resolution)
    if not framerates:
        print("No framerates available for the chosen resolution.")
        chosen_framerate = input("Please enter a framerate: ")
    else:
        for i, framerate in enumerate(framerates):
            print(f"{i+1}. {framerate}")
        framerate_choice = input("Choose a framerate by number: ")
        chosen_framerate = framerates[int(framerate_choice)-1]

    # Mapping from v4l2-ctl formats to GStreamer format strings
    format_mapping = {
        'YUYV 4:2:2': 'video/x-raw, format=(string)YUY2',
        'Motion-JPEG, compressed': 'image/jpeg',
        '16-bit RGB 5-6-5': 'video/x-raw, format=(string)RGB16',
        '8-bit Bayer GRGR/BGBG': 'video/x-raw, format=(string)GRAY8',
        '8-bit Bayer GBGB/RGRG': 'video/x-raw, format=(string)GRAY8',
        'Planar YUV 4:1:1': 'video/x-raw, format=(string)I420',
        '8-bit Bayer BGBG/GRGR': 'video/x-raw, format=(string)GRAY8',
        '8-bit Bayer RGRG/GBGB': 'video/x-raw, format=(string)GRAY8'
    }

    pipeline_format = format_mapping.get(chosen_format, chosen_format)
    chosen_framerate = chosen_framerate.split('-')[0] if '-' in chosen_framerate else chosen_framerate
    if pipeline_format == 'image/jpeg':
        pipeline = f"gst-launch-1.0 v4l2src device={chosen_device} ! '{pipeline_format},width={chosen_resolution.split('x')[0]},height={chosen_resolution.split('x')[1]},framerate={int(float(chosen_framerate.split(' ')[0]))}/1' ! jpegdec ! videoconvert ! autovideosink"
    else:
        pipeline = f"gst-launch-1.0 v4l2src device={chosen_device} ! '{pipeline_format},width={chosen_resolution.split('x')[0]},height={chosen_resolution.split('x')[1]},framerate={int(float(chosen_framerate.split(' ')[0]))}/1' ! videoconvert ! autovideosink"


    print("Your GStreamer pipeline is: ", pipeline)


if __name__ == "__main__":
    main()
