
```
apt-get install  libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
      gstreamer1.0-plugins-base gstreamer1.0-plugins-good \
      gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly \
      gstreamer1.0-libav libgstrtspserver-1.0-dev libges-1.0-dev\
      libgstreamer-plugins-bad1.0-dev pkg-config libssl-dev

```


```
cd /home/pi/src
git clone  https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs
cd gst-plugins-rs
#cargo cbuild -p gst-plugin-ndi --prefix=/usr
#cargo cinstall -p gst-plugin-ndi --prefix=/usr
cd net
cd ndi
cargo build --release
sudo install -o root -g root -m 644 target/release/libgstndi.so /usr/lib/usr/lib/arm-linux-gnueabihf/gstreamer-1.0/
```

###  via https://github.com/teltek/gst-plugin-ndi

```
apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-base

cd ~/src
git clone https://github.com/teltek/gst-plugin-ndi
cd gst-plugin-ndi
cargo build
cargo build --release
sudo install -o root -g root -m 644 target/release/libgstndi.so /usr/lib/usr/lib/arm-linux-gnueabihf/gstreamer-1.0/
```
```
sudo install -o root -g root -m 644 target/release/libgstndi.so /usr/lib/usr/lib/arm-linux-gnueabihf/gstreamer-1.0/
```

###  test

####  webcam+audio test
```
gst-launch-1.0 -v v4l2src device=/dev/video0 ! jpegdec ! video/x-raw ! ndisinkcombiner name=combiner ! ndisink ndi-name="My NDI source"  audiotestsrc is-live=true ! combiner.audio
```
