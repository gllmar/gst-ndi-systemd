import os

def find_v4l2_video_devices():
    video_devices = []

    for entry in os.scandir('/dev'):
        if entry.is_char_device() and entry.name.startswith('video'):
            device_path = entry.path
            name_path = os.path.join(device_path, 'name')

            if os.path.exists(name_path):
                with open(name_path, 'r') as f:
                    name = f.read().strip()
                    video_devices.append((name, device_path))

    return video_devices

def print_video_devices(video_devices):
    if not video_devices:
        print("No video devices found.")
        return

    print("Video Devices:")
    for index, device in enumerate(video_devices):
        name, path = device
        print(f"{index + 1}. Name: {name}")
        print(f"   Device path: {path}")
    print()

def main():
    # Find video devices using V4L2
    video_devices = find_v4l2_video_devices()

    # Print the video devices
    print_video_devices(video_devices)

if __name__ == "__main__":
    main()
