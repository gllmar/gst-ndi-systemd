import subprocess
import re

def run_command(command):
    try:
        return subprocess.check_output(command, shell=True).decode('utf-8')
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {e}")
        return ""

def select_option(options, prompt):
    for i, option in enumerate(options, start=1):
        print(f"{i}. {option}")
    while True:
        try:
            choice = int(input(prompt)) - 1
            if 0 <= choice < len(options):
                return options[choice]
            else:
                print("Invalid selection. Please try again.")
        except ValueError:
            print("Invalid input. Please enter a number.")

# List available devices
devices_output = run_command("v4l2-ctl --list-devices")
devices = re.findall('(.*):\n\t(/dev/video\d+)', devices_output)
selected_device = select_option([f"{d[0]} ({d[1]})" for d in devices], "Select a device: ")

# Use the correct device path for the selected device
device_path = devices[[f"{d[0]} ({d[1]})" for d in devices].index(selected_device)][1].strip()
modes_output = run_command(f"v4l2-ctl --device={device_path} --list-formats-ext")

# Parsing for capture modes, resolutions, and frame rates
modes = re.findall("'(\S+)' \((.+?)\)(.+?)(?=\n\s+\n|\Z)", modes_output, re.DOTALL)
if not modes:
    print("No capture modes found for this device.")
else:
    selected_mode_info = select_option([f"{m[1]} ({m[0]})" for m in modes], "Select a capture mode: ")
    selected_mode_format = selected_mode_info.split(' ')[-1].strip('()')
    selected_mode_name = ' '.join(selected_mode_info.split(' ')[:-1]).strip('()')

    # Find the selected mode's details for parsing resolutions and frame rates
    for mode in modes:
        if mode[0] == selected_mode_format and mode[1].startswith(selected_mode_name):
            mode_details = mode[2]
            break

    # Revised regex to handle multiple intervals per resolution
    resolution_fps_pairs = re.findall('Size: Discrete (\d+x\d+)(.+?)(?=Size: Discrete|\Z)', mode_details, re.DOTALL)
    all_options = []
    for res, fps_details in resolution_fps_pairs:
        fps_values = re.findall('Interval: .*?([\d.]+) fps', fps_details)
        for fps in fps_values:
            all_options.append((res, fps))

    if all_options:
        selected_resolution_info = select_option([f"{r[0]} at {r[1]} fps" for r in all_options], "Select a resolution and frame rate: ")
        selected_resolution, selected_frame_rate = selected_resolution_info.split(' at ')
    else:
        print("No resolutions and frame rates found for this mode.")

# After user selects resolution and frame rate
selected_resolution, selected_frame_rate = selected_resolution_info.split(' at ')
# Ensure the frame rate is formatted correctly
selected_frame_rate = selected_frame_rate.split('.')[0]

# Construct the GStreamer pipeline command
gst_command = f"gst-launch-1.0 -v v4l2src device={device_path} ! videoconvert ! video/x-raw,format=UYVY,width={selected_resolution.split('x')[0]},height={selected_resolution.split('x')[1]},framerate={selected_frame_rate}/1 ! ndisinkcombiner name=combiner alsasrc device=\"hw:5,0\" ! audioconvert ! combiner.audio combiner.src ! queue ! ndisink ndi-name=\"My NDI source\""
print("\nGStreamer Pipeline Command:")
print(gst_command)
