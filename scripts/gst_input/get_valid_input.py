import subprocess

# Retrieve capture devices and their resolutions
output = subprocess.check_output(['v4l2-ctl', '--list-devices']).decode('utf-8')
devices = [line.split(':')[0].strip() for line in output.split('\n') if '/dev/video' in line]

# Check if any capture devices are found
if not devices:
    print("No capture devices found.")
    exit(1)

# Function to parse resolutions for a given device
def get_resolutions(device):
    output = subprocess.check_output(['v4l2-ctl', '--list-formats-ext', '--device', device]).decode('utf-8')
    resolutions = [line.split(':')[1].strip() for line in output.split('\n') if 'Size:' in line]
    return resolutions

# Create a menu for selecting a device with valid resolutions
print("Listing capture devices...\n")

selected_device = None
while not selected_device:
    print("Available Capture Devices:\n")
    for i, device in enumerate(devices, start=1):
        try:
            pretty_name = subprocess.check_output(['v4l2-ctl', '--list-devices', device]).decode('utf-8').strip()
        except subprocess.CalledProcessError:
            pretty_name = f"Device {device} (Cannot be opened)"
        print(f"{i}) {pretty_name}")

    device_number = input("Select a capture device (enter the corresponding number): ")

    try:
        device_number = int(device_number)
        if device_number < 1 or device_number > len(devices):
            raise ValueError
    except ValueError:
        print("Invalid selection.")
    else:
        device_index = device_number - 1
        selected_device = devices[device_index]
        selected_pretty_name = subprocess.check_output(['v4l2-ctl', '--list-devices', selected_device]).decode('utf-8').strip()
        selected_resolutions = get_resolutions(selected_device)

print(f"\nSelected Capture Device: {selected_pretty_name}")

print("\nSupported Resolutions for", selected_pretty_name + ":")
for i, resolution in enumerate(selected_resolutions, start=1):
    print(f"{i}) {resolution}")
