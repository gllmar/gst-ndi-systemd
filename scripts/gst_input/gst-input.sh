#!/bin/bash

output=$(v4l2-ctl --list-devices)

# Remove leading whitespace
output=$(echo "$output" | sed -e 's/^[[:space:]]*//')

# Set field separator to newline
IFS=$'\n'

# Initialize variables
devices=()

# Loop through each line of the output
while IFS= read -r line; do
  if [[ $line =~ ^[[:alnum:]] ]]; then
    # Found a line with device name
    device_name=$line
  elif [[ $line =~ ^/dev/video[[:digit:]] ]]; then
    # Found a line with device path
    device_path=$line

    # Add device to the list
    devices+=("$device_path $device_name")
  fi
done <<< "$output"

# Sort the devices
sorted_devices=($(printf '%s\n' "${devices[@]}" | sort))

# Function to list available resolutions for a device
function list_resolutions() {
  device_name=$1
  device_path=$2

  formats_ext=$(v4l2-ctl --list-formats-ext -d "$device_path")

  echo "Available Resolutions for $device_path $device_name:"
  count=1
  resolutions=()
  frame_rates=()
  while IFS= read -r line; do
    if [[ $line =~ ^[[:space:]]+Size:[[:space:]]*Discrete[[:space:]]+([[:digit:]]+x[[:digit:]]+) ]]; then
      resolution=${BASH_REMATCH[1]}
      resolutions+=("$resolution")
      echo "$count. $resolution"
    elif [[ $line =~ ^[[:space:]]+Interval: ]]; then
      frame_rate=$(echo "$line" | grep -oP '\((\d+.\d+-\d+.\d+) fps\)' | sed -E 's/[( fps)]//g' | awk -F '-' '{printf "%.0f\n", $1}')
      frame_rates+=("$frame_rate")
      echo "   Frame Rate: $frame_rate"
      ((count++))
    fi
  done <<< "$formats_ext"
  echo

  read -p "Select a resolution (enter the corresponding number): " resolution_choice

  if (( resolution_choice >= 1 && resolution_choice <= ${#resolutions[@]} )); then
    selected_resolution=${resolutions[$(($resolution_choice-1))]}
    selected_frame_rate=${frame_rates[$(($resolution_choice-1))]}
    echo "Selected Resolution: $selected_resolution"
    echo "Selected Frame Rate: $selected_frame_rate"

    # Compose GStreamer pipeline
    pipeline="v4l2src device=$device_path ! video/x-raw,format=YUY2,width=${selected_resolution%x*},height=${selected_resolution#*x},framerate=$selected_frame_rate/1 ! queue ! videoconvert ! autovideosink"
    echo "GStreamer Pipeline: $pipeline"

    # Run the GStreamer pipeline
    gst-launch-1.0 "$pipeline"
  else
    echo "Invalid choice. Please try again."
    list_resolutions "$device_name" "$device_path"
  fi
}





# Function to display the menu and process user selection
function display_menu() {
  echo "Video Devices:"
  for (( i=0; i<${#sorted_devices[@]}; i++ )); do
    device=(${sorted_devices[$i]})
    echo "$(($i+1)). ${device%% *} ${device#* }"
  done
  echo "0. Exit"

  echo
  read -p "Select a device (enter the corresponding number): " choice

  if [[ $choice == "0" ]]; then
    echo "Exiting..."
    exit
  elif (( choice >= 1 && choice <= ${#sorted_devices[@]} )); then
    selected_device=(${sorted_devices[$(($choice-1))]})

    device_path=${selected_device%% *}
    device_name=${selected_device#* }

    list_resolutions "$device_name" "$device_path"
  else
    echo "Invalid choice. Please try again."
    display_menu
  fi
}

# Display the menu
display_menu
