#!/bin/bash

# Get a list of video capture devices using v4l2-ctl
devices=$(v4l2-ctl --list-devices | grep -oP '/dev/video\d+')

# Store the devices in an array
IFS=$'\n' read -rd '' -a device_array <<<"$devices"

# Print the menu
echo "Select a video capture device:"

# Loop through the array and print the menu options
for i in "${!device_array[@]}"; do
    echo "$i. ${device_array[i]}"
done

# Read user input
read -p "Enter the number corresponding to the device: " choice

# Validate user input
if [[ $choice =~ ^[0-9]+$ ]] && [ $choice -ge 0 ] && [ $choice -lt ${#device_array[@]} ]; then
    selected_device=${device_array[choice]}
    echo "You selected: $selected_device"
else
    echo "Invalid choice!"
fi
