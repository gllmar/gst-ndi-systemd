import subprocess
import re

def select_resolution():
    # Run the command and capture the output
    command = 'v4l2-ctl --list-formats-ext -d "/dev/video0" | grep -A 10 "Size: "'
    output = subprocess.check_output(command, shell=True).decode('utf-8')
    
    # Parse the output and extract the available resolutions
    resolutions = []
    pattern = re.compile(r'Size: Discrete (\d+x\d+)')
    matches = pattern.findall(output)
    for match in matches:
        resolutions.append(match)
    
    # Display the available resolutions and let the user choose
    print("Available Resolutions:")
    for i, resolution in enumerate(resolutions):
        print(f"{i+1}. {resolution}")
    
    while True:
        selection = input("Enter the number corresponding to your desired resolution: ")
        if selection.isdigit() and 1 <= int(selection) <= len(resolutions):
            return resolutions[int(selection)-1]
        else:
            print("Invalid selection. Please try again.")

def select_frame_rate(resolution):
    # Run the command and capture the output
    command = f'v4l2-ctl --list-formats-ext -d "/dev/video0" | grep -A 10 "Size: {resolution}"'
    output = subprocess.check_output(command, shell=True).decode('utf-8')

    # Parse the output and extract the frame rates for the selected resolution
    frame_rates = []
    pattern = re.compile(r'Interval: Discrete \d+.\d+s \((\d+.\d+) fps\)')
    matches = pattern.findall(output)
    for match in matches:
        frame_rates.append(match)
    
    return frame_rates

# Main script
selected_resolution = select_resolution()
selected_frame_rates = select_frame_rate(selected_resolution)

print(f"\nSelected Resolution: {selected_resolution}")
print("Available Frame Rates:")
for i, frame_rate in enumerate(selected_frame_rates):
    print(f"{i+1}. {frame_rate} fps")
