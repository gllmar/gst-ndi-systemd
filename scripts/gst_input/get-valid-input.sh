#!/bin/bash

# Function to parse frame rates for a given resolution
parse_frame_rates() {
  local selected_device=$1
  local selected_resolution=$2

  # Get supported frame rates for the selected resolution
  frame_rates_raw=$(v4l2-ctl --list-formats-ext -d "$selected_device" | grep -A 10 "Size: $selected_resolution" | grep "Interval: ")
  frame_rates=($(echo "$frame_rates_raw" | awk -F '[(,]' '{print $2}' | awk '!seen[$0]++'))

  # Return the frame rates
  echo "${frame_rates[@]}"
}

# Index all valid capture devices
devices=($(ls /dev/video* 2>/dev/null))
device_count=${#devices[@]}

if [ $device_count -eq 0 ]; then
  echo "No capture devices found."
  exit 1
fi

# Display capture devices menu
echo "Select a capture device:"
for ((i=0; i<$device_count; i++)); do
  echo "[$i] ${devices[$i]}"
done

read -p "Enter the device number: " device_index

# Validate device index
if [[ ! $device_index =~ ^[0-9]+$ ]] || [ $device_index -lt 0 ] || [ $device_index -ge $device_count ]; then
  echo "Invalid device index."
  exit 1
fi

selected_device=${devices[$device_index]}

# Get supported capture resolutions
formats=$(v4l2-ctl --list-formats-ext -d "$selected_device")

# Extract resolutions
resolutions=($(echo "$formats" | grep "Size: " | awk '{print $NF}'))
resolution_count=${#resolutions[@]}

# Display resolutions menu
echo "Selected device: $selected_device"
echo "Select a resolution:"
for ((i=0; i<$resolution_count; i++)); do
  echo "[$i] ${resolutions[$i]}"
done

read -p "Enter the resolution number: " resolution_index

# Validate resolution index
if [[ ! $resolution_index =~ ^[0-9]+$ ]] || [ $resolution_index -lt 0 ] || [ $resolution_index -ge $resolution_count ]; then
  echo "Invalid resolution index."
  exit 1
fi

selected_resolution=${resolutions[$resolution_index]}

# Parse frame rates for the selected resolution
selected_frame_rates=($(parse_frame_rates "$selected_device" "$selected_resolution"))
frame_rate_count=${#selected_frame_rates[@]}

# Display frame rates menu
echo "Selected resolution: $selected_resolution"
echo "Select a frame rate:"
for ((i=0; i<$frame_rate_count; i++)); do
  echo "[$i] ${selected_frame_rates[$i]} fps"
done

read -p "Enter the frame rate number: " frame_rate_index

# Validate frame rate index
if [[ ! $frame_rate_index =~ ^[0-9]+$ ]] || [ $frame_rate_index -lt 0 ] || [ $frame_rate_index -ge $frame_rate_count ]; then
  echo "Invalid frame rate index."
  exit 1
fi

selected_frame_rate=${selected_frame_rates[$frame_rate_index]}

# Print the selected device, resolution, and frame rate
echo "Selected capture device: $selected_device"
echo "Selected resolution: $selected_resolution"
echo "Selected frame rate: $selected_frame_rate fps"
