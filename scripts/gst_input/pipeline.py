import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

def create_pipeline(device, resolution, framerate):
    pipeline = Gst.Pipeline()

    # Create elements
    source = Gst.ElementFactory.make('v4l2src', 'source')
    capsfilter = Gst.ElementFactory.make('capsfilter', 'capsfilter')
    videoconvert = Gst.ElementFactory.make('videoconvert', 'videoconvert')
    videosink = Gst.ElementFactory.make('autovideosink', 'videosink')

    # Set device properties
    source.set_property('device', device)

    # Set resolution
    caps = Gst.Caps.from_string(f'video/x-raw, width={resolution[0]}, height={resolution[1]}')
    capsfilter.set_property('caps', caps)

    # Set framerate
    source_caps = Gst.Caps.from_string('video/x-raw')
    source_caps.set_simple('framerate', Gst.Fraction(framerate, 1))
    source.set_property('caps', source_caps)

    # Add elements to the pipeline
    elements = [source, capsfilter, videoconvert, videosink]
    for element in elements:
        pipeline.add(element)

    # Link elements
    source.link(capsfilter)
    capsfilter.link(videoconvert)
    videoconvert.link(videosink)

    return pipeline

def prompt_user():
    device = input("Enter video capture device (e.g., /dev/video0): ")
    resolution_str = input("Enter resolution (e.g., 1920x1080): ")
    framerate = int(input("Enter framerate (e.g., 30): "))

    resolution = tuple(map(int, resolution_str.split('x')))
    pipeline = create_pipeline(device, resolution, framerate)
    print("GStreamer pipeline created successfully!")

    return pipeline

if __name__ == '__main__':
    pipeline = prompt_user()

    # Run the pipeline
    pipeline.set_state(Gst.State.PLAYING)

    # Wait until termination
    bus = pipeline.get_bus()
    msg = bus.timed_pop_filtered(Gst.CLOCK_TIME_NONE, Gst.MessageType.ERROR | Gst.MessageType.EOS)
    pipeline.set_state(Gst.State.NULL)
