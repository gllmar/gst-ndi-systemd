#!/bin/bash
#SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

export DISPLAY=:0

#cd "$SCRIPTPATH"

gst-launch-1.0 -v \
ximagesrc startx=0 use-damage=0  \
! video/x-raw,framerate=60/1,width=480,height=800  \
! ndisinkcombiner name=combiner ! \
ndisink ndi-name="gst-ndi" \
pulsesrc ! audioconvert ! audioresample ! queue ! combiner.audio 

