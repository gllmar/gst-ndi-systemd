#!/bin/bash

# Use environment variables if set, else use default values
loopback_device=${LOOPBACK_DEVICE:-"62"}
filter_string=${FILTER_STRING:-""}
ndi_width=${NDI_WIDTH:-"1280"}  # Default width
ndi_height=${NDI_HEIGHT:-"720"} # Default height
fullscreen_enabled=false  # Default to false

# Parse arguments
while getopts "d:n:w:h:fa" opt; do
    case "$opt" in
        d) loopback_device="$OPTARG";;
        n) filter_string="$OPTARG";;
        w) ndi_width="$OPTARG";;
        h) ndi_height="$OPTARG";;
        f) fullscreen_enabled=true;;
        a) echo "The -a option is placeholder and currently not used in this script version.";;
        \?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
    esac
done

echo_usage(){
        echo "Usage: $0 -d <loopback_device> -n <filter_string> [-w <width> -h <height>] [-f]"
}

# If no arguments are provided, list available NDI sources and exit
if [ "$#" -eq 0 ]; then
    echo_usage
    echo "Available NDI sources:"
    timeout 2s gst-device-monitor-1.0 -f Source/Network:application/x-ndi 2>/dev/null | grep -B 4 "$filter_string" | grep "ndi-name" | cut -d "=" -f2- | tr -d "[:space:]" | awk '{gsub(/\.\.\./, "\n")}1'
    #timeout 2s gst-device-monitor-1.0 -f Source/Network:application/x-ndi 2>/dev/null | grep -B 4 "$filter_string" | grep "ndi-name" | cut -d "=" -f2- | tr -d "[:space:]" | sed 's/!/\n/g'
    exit 0
fi

# Check required arguments
if [ -z "$loopback_device" ] || [ -z "$filter_string" ]; then
    echo_usage
    exit 1
fi

# Function to log incoming resolution
log_incoming_resolution() {
    local ndi_url="$1"

    echo "Checking incoming video resolution..."

    # Launch a pipeline that outputs the caps, then extract the video resolution from caps
    local caps=$(timeout 1s gst-launch-1.0 -v ndisrc url-address="$ndi_url" ! ndisrcdemux name=demux demux.video ! queue ! fakesink 2>&1 | grep -o 'width=(int)[0-9]*, height=(int)[0-9]*')

    if [[ ! -z "$caps" ]]; then
        # Extract the resolution using Perl-compatible regex
        local width=$(echo "$caps" | grep -oP 'width=\(int\)\K[0-9]*' | head -n 1)
        local height=$(echo "$caps" | grep -oP 'height=\(int\)\K[0-9]*' | head -n 1)
        
        if [[ ! -z "$width" && ! -z "$height" ]]; then
            local resolution="${width}x${height}"
            echo "Incoming video resolution detected: $resolution"
        else
            echo "Could not determine incoming video resolution."
        fi
    else
        echo "Could not determine incoming video resolution."
    fi
}



# Function to start the pipeline
start_pipeline() {
    local ndi_url="$1"
    local ndi_width="$2"
    local ndi_height="$3"
    local loopback_device="$4"

    # Construct the GStreamer pipeline
    local pipeline="gst-launch-1.0 ndisrc url-address=$ndi_url ! ndisrcdemux name=demux demux.video ! queue ! videoconvert ! videoscale ! video/x-raw,format=YUY2,width=$ndi_width,height=$ndi_height ! v4l2sink device=/dev/video$loopback_device"    
    echo "Starting pipeline: $pipeline"
    eval "$pipeline" &

    local pipeline_pid=$!

    # Start ffplay if fullscreen is enabled
    if $fullscreen_enabled; then
        ffplay -f v4l2 -i /dev/video$loopback_device >/dev/null 2>&1 &
        local ffplay_pid=$!
    fi

    wait $pipeline_pid
    kill $ffplay_pid 2>/dev/null
}

# Extract information about all NDI sources
ndi_sources_info=$(timeout 2s gst-device-monitor-1.0 -f Source/Network:application/x-ndi 2>/dev/null)

# Find the first matching NDI source based on filter string and extract its name and URL
ndi_name=$(echo "$ndi_sources_info" | grep -B 4 "$filter_string" | grep "ndi-name" | head -n 1 | cut -d "=" -f2- | tr -d "[:space:]")
ndi_url=$(echo "$ndi_sources_info" | grep -A 1 "$filter_string" | grep "url-address" | head -n 1 | cut -d "=" -f2- | tr -d "[:space:]")

# Check if NDI name and URL were found
if [ -z "$ndi_url" ] || [ -z "$ndi_name" ]; then
    echo "No matching NDI source found."
    exit 1
fi

# Print the complete name of the found NDI stream
echo "Found NDI stream: $ndi_name with URL: $ndi_url"

# Add a call to log_incoming_resolution before starting the main pipeline
log_incoming_resolution "$ndi_url"

# Start the pipeline with the specified resolution
start_pipeline "$ndi_url" "$ndi_width" "$ndi_height" "$loopback_device"
