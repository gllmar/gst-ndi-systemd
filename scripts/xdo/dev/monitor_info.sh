#!/bin/bash

# Get monitor information using xrandr
monitor_info=$(xrandr --query)

# Use awk to filter and print monitor names, resolutions, and offsets
echo "$monitor_info" | awk '/ connected/ {
    print "Monitor:", $1;
    for (i = 1; i <= NF; i++) {
        if ($i ~ /^[0-9]+x[0-9]+\+[0-9]+\+[0-9]+$/) {
            resolution_offset = $i;
            split(resolution_offset, resolution_offset_parts, /[+]/);
            resolution = resolution_offset_parts[1];
            offset = resolution_offset_parts[2] " " resolution_offset_parts[3];
            print "Resolution:", resolution;
            print "Offset:", offset;
            break;
        }
    }
}'
