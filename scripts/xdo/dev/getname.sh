#!/bin/bash

window_ids=$(xdotool search --onlyvisible --name ".*")
for window_id in $window_ids; do
    window_name=$(xdotool getwindowname "$window_id")
    if [[ "$window_name" != "" ]]; then
        echo "$window_name"
    fi
done
