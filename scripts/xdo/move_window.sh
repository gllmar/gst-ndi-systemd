#!/bin/bash

# Function to get the geometry (position and size) of a monitor
get_monitor_geometry() {
    local monitor_name=$1
    xrandr_output=$(xrandr --query)
    monitor_info=$(echo "$xrandr_output" | awk -v name="$monitor_name" '/ connected/ && $1 == name { print $0 }')

    # Exclude the "primary" keyword from monitor_info
    monitor_info=$(echo "$monitor_info" | sed 's/primary //')

    resolution=$(echo "$monitor_info" | awk '{ split($3, res_parts, /[+x]/); print res_parts[1], res_parts[2] }')

    offset_info=$(echo "$monitor_info" | awk -F '[+]' '{ print $2, $3 }')
    offset_x=$(echo "$offset_info" | awk '{ print $1 }')
    offset_y=$(echo "$offset_info" | awk '{ print $2 }')

    echo "$resolution $offset_x $offset_y"
}

# Function to move the window to the selected monitor
move_window_to_monitor() {
    local window_name=$1
    local monitor_name=$2

    # Get the window ID based on the window name
    window_id=$(wmctrl -l | awk -v name="$window_name" '$0 ~ name { print $1 }')

    if [[ -n $window_id ]]; then
        echo "Window ID: $window_id"

        # Get the monitor resolution and offset
        geometry=$(get_monitor_geometry "$monitor_name")

        if [[ -n $geometry ]]; then
            resolution=$(echo "$geometry" | awk '{ print $1, $2 }')
            offset_x=$(echo "$geometry" | awk '{ print $3 }')
            offset_y=$(echo "$geometry" | awk '{ print $4 }')

            echo "Monitor Resolution: $resolution"
            echo "Monitor Offset: $offset_x $offset_y"

            # Compose the wmctrl command
            wmctrl_command="wmctrl -r '$window_name' -e '0,$offset_x,$offset_y,-1,-1'"
            echo "wmctrl command: $wmctrl_command"

            # Move the window to the selected monitor
            eval "$wmctrl_command"
            echo "Moved window '$window_name' to monitor '$monitor_name'."
        else
            echo "Monitor '$monitor_name' not found."
        fi
    else
        echo "Window '$window_name' not found."
    fi
}

# Read the window name and monitor name from command-line arguments
window_name=$1
monitor_name=$2

# Validate the input arguments
if [[ -z $window_name ]]; then
    echo "Window name is required."
    exit 1
fi

if [[ -z $monitor_name ]]; then
    echo "Monitor name is required."
    exit 1
fi

# Move the window to the selected monitor
move_window_to_monitor "$window_name" "$monitor_name"
