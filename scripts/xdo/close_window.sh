#!/bin/bash

# Function to close the window by its name
close_window_by_name() {
    local window_name=$1

    # Get the window ID based on the window name
    window_id=$(wmctrl -l | awk -v name="$window_name" '$0 ~ name { print $1 }')

    if [[ -n $window_id ]]; then
        # Close the window using wmctrl
        wmctrl -ic "$window_id"
        echo "Closed window '$window_name'."
    else
        echo "Window '$window_name' not found."
    fi
}

# Read the window name from command-line argument
window_name=$1

# Validate the input argument
if [[ -z $window_name ]]; then
    echo "Window name is required."
    exit 1
fi

# Close the window by its name
close_window_by_name "$window_name"
